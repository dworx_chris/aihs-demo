import React from 'react';
import { Stack, IStackTokens } from 'office-ui-fabric-react';
import { ChoiceGroup } from 'office-ui-fabric-react/lib/ChoiceGroup';
import { Label } from 'office-ui-fabric-react/lib/Label';
import { PrimaryButton } from 'office-ui-fabric-react';
import { Text } from 'office-ui-fabric-react/lib/Text';
import { Separator } from 'office-ui-fabric-react/lib/Separator';
import { Dropdown, IDropdownStyles, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';

// ----------------------------------------------------------
// Styles definition


const VERTICAL_STACK_TOKEN: IStackTokens = {
  padding: 's1',
  childrenGap: 'm',
  
};

const QUESTION_LABEL_STACK_ITEM_STYLE: IStackItemStyles = {
  root: {
    alignItems: 'center',
    display: 'flex',
    overflow: 'hidden',
    width: 400
  }
};

const DROPDOWN_STYLE: Partial<IDropdownStyles> = {
  dropdown: { width: 300 }
};

// End of Styles definition
// ----------------------------------------------------------

function Title(data) {
	return <Text variant="xLarge"
			style={{
				padding: "20px 0px 10px 0px"
			}}>
			{data.text} 
		</Text>
}

const choiceFieldStyle = {
	root: {
		padding: '5px 10px 5px 0px'
	}
}

const questionStackStyle = {
		margin: "5px 0px 0px 0px"
	}

const scale5options = [
  	{ 	key: "0", 
  		value: 1,
  		text: "0 (No problem)", 
 		styles: choiceFieldStyle
 	},
  	{ 	key: "1", 
  		value: 1,
  		text: "1 (Minor not requiring attention)",
  		styles: choiceFieldStyle
	},
	{ 	key: "2", 
		value: 1,
		text: "2 (Mild but present)",
		styles: choiceFieldStyle
	},
	{ 
		key: "3", 
		value: 1,
		text: "3 (Moderate)",
		styles: choiceFieldStyle
	},
	{ 	key: "4",
		value: 1, 
		text: "4 (Severe to very severe)",
		styles: choiceFieldStyle
	},
	{ 	key: "9",
		value: 0, 
		text: "9 (Unknown or not asked)",
		styles: choiceFieldStyle
	}
];

class HonosForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			questions: this.props.data.questions,
			total: 0,
			disableSubmit: true
		};

		this._onSubmit = this._onSubmit.bind(this);
		this._onChoiceGroupChanged = this._onChoiceGroupChanged.bind(this);
		this._onDropdownChanged = this._onDropdownChanged.bind(this);

		this.UpdateState = this.UpdateState.bind(this);
		this.GetFormElement = this.GetFormElement.bind(this);
		this.GetFormsUIElement = this.GetFormsUIElement.bind(this);
	}


	GetFormElement(item) {
		if(item.type === "scale5") {

			var dropdown = null;
			if(item.dropdown) {
				const dropdownOptions: IDropdownOption[] = item.dropdown.options;
				
				dropdown = <Stack horizontal>
								<Dropdown
									placeholder={item.dropdown.label}
							        options={dropdownOptions}
							        styles={DROPDOWN_STYLE}
							        onChange={(e, option, index) => this._onDropdownChanged(e, option, item.id)}
							        />
							</Stack>
			}

			return <Stack key={item.id} vertical
					style={questionStackStyle}>
				<Label 
					disableShrink 
					styles={QUESTION_LABEL_STACK_ITEM_STYLE}>
					{item.id}. {item.label}
				</Label>
				{dropdown}
				<ChoiceGroup
					className="defaultChoiceGroup"
	          		name={item.id}
	          		key={item.id}
	          		options={scale5options}
	          		onChange={(e, options) => this._onChoiceGroupChanged(e, options, item.id)}
	          		styles={
	          			{

	          				flexContainer: {
			        		 	display: "flex", 
			        		 	justifyContent: "space-between"
		        			}
		        		}
		        	}
		        />
		        <Separator styles={{padding: "100px"}}/>
		    </Stack>
		}

		return null;
	}


	GetFormsUIElement() {
		const questionsUI = this.props.data.questions.map((item) => {
			return this.GetFormElement(item)
		});
		return questionsUI;
	}

	UpdateState(selectedQuestion, updatedValue) {
		// Update questions object
		const questions = this.state.questions.map((q) => {
			if(q.id === selectedQuestion.id) {
				return Object.assign({}, q, updatedValue);
			}
			return q;
		});

		// Update total value
		const total = questions.reduce((acc, q) => {
			if(q.value !== "") {
				return acc + parseInt(q.value);
			} 
			return acc;
		}, 0);

		// Validation check
		const disableSubmit = questions.reduce((acc, q) => {
			if(q.dropdown && q.dropdown.value === undefined) {
				return acc || (q.dropdown.value === undefined);
			} else {
				return acc || (q.selectedValue === undefined );
			}
		}, false)

		this.setState({
			questions: questions,
			total: total,
			disableSubmit: disableSubmit
		});
	}

	_onDropdownChanged(e, option, selectedQuestionId) {
		const filteredQuestions = this.state.questions.filter((q) => {
			return q.id === selectedQuestionId;
		});

		// make sure that we only get 1 selected question from the state variable
		if(filteredQuestions.length === 1 ) {
			const selectedQuestion = filteredQuestions[0];
			const dropdown = Object.assign({}, selectedQuestion.dropdown, {
				"value": option.value,
				selectedKey: option.key
			});

			// Update the selection value, so it can be used to update the total
			let value = "";
			if(selectedQuestion.selectedValue 
				&& selectedQuestion.selectedValue !== "") {
				value = String(parseInt(selectedQuestion.selectedValue) * parseInt(option.value));
			}
			
			const updatedValue = {
				dropdown: dropdown,
				value: value
			}

			this.UpdateState(selectedQuestion, updatedValue);
		}		
	}

	_onChoiceGroupChanged(e, option, selectedQuestionId) {
		const filteredQuestions = this.state.questions.filter((q) => {
			return q.id === selectedQuestionId;
		});

		// make sure that we only get 1 selected question from the state variable
		if(filteredQuestions.length === 1 ) {
			const selectedQuestion = filteredQuestions[0];
			const selectedValue = parseInt(option.value) * parseInt(option.key);
			let value = selectedValue;
			
			if(selectedQuestion.dropdown) {
				// User must select the dropdown before a value will be assigned
				value = (selectedQuestion.dropdown.value !== "" && selectedQuestion.dropdown.value !== undefined) ? value * parseInt(selectedQuestion.dropdown.value) : 0;
			}

			const updatedValue = {
				value: String(value),
				selectedValue: String(selectedValue),
				selectedKey: option.key
			}

			this.UpdateState(selectedQuestion, updatedValue);
		}

		
	}


	_onSubmit(e) {
		const questionData = this.state.questions.map((q) => {
			let data = {
				id: q.id,
				selectedChoice: q.selectedKey,
				value: q.value
			}

			if(q.dropdown) {
				data.selectedDropdown = q.dropdown.selectedKey
			}
			return data;
		})

		const submittedData = {
			questions: questionData,
			total: this.state.total
		}

		alert("Ready to submit following data:\n" + JSON.stringify(submittedData));
		e.preventDefault();

	}

	render() {
		return (
			<form>

				<Stack vertical tokens={VERTICAL_STACK_TOKEN}>
				
					<Title text={this.props.data.title}/>
					
				    {this.GetFormsUIElement()}
				    <Text variant="xLarge" 
				    	style={{margin: "0px"}}>
						Total: {this.state.total} 
					</Text>
				    <Stack horizontal >
					    <PrimaryButton
				            data-automation-id="submit"
				            text="Submit"
				            disabled={this.state.disableSubmit}
				            onClick={this._onSubmit}
				          />
					</Stack>			
				</Stack>
			</form>
		)
	}
}

export default HonosForm;