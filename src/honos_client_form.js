import React from 'react';
import { initializeIcons } from 'office-ui-fabric-react/lib/Icons';
import { TextField, MaskedTextField } from 'office-ui-fabric-react/lib/TextField';
import { Stack, IStackTokens } from 'office-ui-fabric-react';
import { DatePicker, IDatePickerStrings, IDatePickerStyles } from 'office-ui-fabric-react';
import { Dropdown, IDropdownStyles, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import { PrimaryButton } from 'office-ui-fabric-react';
import { Text } from 'office-ui-fabric-react/lib/Text';
import moment from 'moment'

const SITE_INFO = "site_info";
const CLIENT_INFO = "client_info";
const EXTRA_CLIENT_INFO = "extra_client_info";

// ----------------------------------------------------------
// Styles definition

const TEXTFIELD_WIDTH = 300;

const HORIZONTAL_STACK_TOKEN: IStackTokens = {
  childrenGap: 'm',
  padding: 's1'
  
};
const VERTICAL_STACK_TOKEN: IStackTokens = {
  padding: 'm'
  
};

const DROPDOWN_STYLE: Partial<IDropdownStyles> = {
  dropdown: { width: TEXTFIELD_WIDTH }
};

const DATEPICKER_STYLE: Partial<IDatePickerStyles> = {
 
};



const DAY_PICKER_STRINGS: IDatePickerStrings = {
	months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],

  	shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],

  	days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],

  	shortDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],

	isRequiredErrorMessage: 'Field is required.',
	invalidInputErrorMessage: 'Invalid date format.'
};

// End of Styles definition
// ----------------------------------------------------------

function Title(data) {
	return <Text variant="xLarge">
			{data.text} 
		</Text>
}

function validate(value, type) {
	if(type === "email") {
		var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	if(re.test(String(value).toLowerCase())) {
    		return "";
    	} else {
    		return "Invalid email";
    	}
	}
	return "";
}

class Client extends React.Component {
	constructor(props) {
		super(props);
		let initialState = {};
		let disableSubmitButton = false;
		props.data.forEach((item) => {
			initialState[item.key] = item.value

			// a bit hack for demo purpose only
			if(item.dependency && item.type === "datepicker") {
				if(item.value === "") {
					initialState[item.dependency.key] = "";	
				} else {
					const momentDate = moment(item.value);
					const age = parseInt(moment.duration(moment().diff(momentDate)).asYears());
					initialState[item.dependency.key] = age;	
				}
			}

			disableSubmitButton = disableSubmitButton || (item.value === "" || item.value === undefined);
			
		});
		initialState.disableSubmit = disableSubmitButton;
		this.state = initialState;

		this._validateOnFocusOut = this._validateOnFocusOut.bind(this);
		this._onDropdownChanged = this._onDropdownChanged.bind(this);
		this._onTextFieldChanged = this._onTextFieldChanged.bind(this);
 		this._onDatePickerChanged = this._onDatePickerChanged.bind(this);
 		this._onSubmit = this._onSubmit.bind(this);

 		this.GetUIComponentsByCategory = this.GetUIComponentsByCategory.bind(this);
 		this.GetFormElement = this.GetFormElement.bind(this);
 		this.ValidateForm = this.ValidateForm.bind(this);
 		initializeIcons();
	}

	
	ValidateForm(currentFormValue) {
		return this.props.data.reduce((acc, item) => {
			const currentItemValue = currentFormValue[item.key];
			if(currentItemValue === "") {
				return acc && false;
			} else if(item.input_type !== undefined && item.input_type !== "") {
				return acc && (validate(currentItemValue, item.input_type) === "")
			} else {
				return acc && true;
			}
		}, true); 
	}

	GetFormElement(item) {
		if(item.key in this.state) {
			if(item.type === "textfield") {
				return <TextField
							key={item.key}
							name={item.key}
							label={item.label}
							value={this.state[item.key]}
							onChange={(e) => this._onTextFieldChanged(item.key, e.target.value)}
							styles={{ fieldGroup: { width: TEXTFIELD_WIDTH } }}
				        	onGetErrorMessage={(value) => this._validateOnFocusOut(value, item.input_type)}
              				validateOnFocusOut
				        />
			} else if(item.type === "dropdown") {
				const dropdownOptions: IDropdownOption[] = item.options;
				return <Dropdown
							key={item.key}
							name={item.key}
					        placeholder={item.label}
					        label={item.label}
					        onChange={(e, option, index) => this._onDropdownChanged(item.key, option, index)}
					        options={dropdownOptions}
					        defaultSelectedKey={this.state[item.key]}
					        styles={DROPDOWN_STYLE}
				      	/>
			} else if(item.type === "datepicker") {
				const date = (this.state[item.key] !== "") ? moment(this.state[item.key]).toDate() : null;
				const datePicker = <DatePicker 
							key={item.key}
				        	strings={DAY_PICKER_STRINGS} 
				        	placeholder={item.label}
				        	ariaLabel={item.label} 
				        	label={item.label}
				        	value={date}
				        	onSelectDate={(date) => this._onDatePickerChanged(item.key, date, item.dependency)}
				        	styles={DATEPICKER_STYLE}
			        	/>

			    if(item.dependency) {
			    	const ageTextField = <TextField
							key={item.dependency.key}
							name={item.dependency.key}
							label={item.dependency.label}
							value={this.state[item.dependency.key]}
							readOnly
				        />
				    return [datePicker, ageTextField];
			    } else {
			    	return datePicker
			    } 	
			    

			} else if(item.type === "masktextfield") {
				return <MaskedTextField 
							key={item.key}
							label={item.label} 
							value={this.state[item.key]}
							onChange={(e) => this._onTextFieldChanged(item.key, e.target.value)}
							mask="(999) 999 - 9999" 
							styles={{ fieldGroup: { width: TEXTFIELD_WIDTH } }}
							/>
			}
		}
		return null
	}


	GetUIComponentsByCategory(category) {
		const categoryUI = this.props.data.map((item) => {
			if(item.category === category) {
				return this.GetFormElement(item);
			}
			return null
		});

		return categoryUI;
	}

	_onDropdownChanged(key, selectedValue) {
		if(key) {
			const currentState = Object.assign({}, this.state, {
				[key]: selectedValue.key
			});

			let isValid =  this.ValidateForm(currentState);
			this.setState({
				[key]: selectedValue.key,
				disableSubmit: !isValid
			});
		}
	}

	_onDatePickerChanged(key, dateValue, dependency) {
		if(key && moment(dateValue)) {
			let date = moment(dateValue);
			let updatedValue = {
				[key]: date.format("YYYY-MM-DD")
			};

			if(dependency) {
				// HACK: Don't do this is real life, demo only for studying react
				// assuming only 1 date picker in the list, that is tied to age
				let duration = moment.duration(moment().diff(date));
				updatedValue["client_age"] = parseInt(duration.asYears());
			}

			const currentState = Object.assign({}, this.state, updatedValue);
			let isValid =  this.ValidateForm(currentState);
			updatedValue["disableSubmit"] = !isValid

			this.setState(updatedValue);
		}
	}

	_onTextFieldChanged(key, value) {
		if(key) {
			const currentState = Object.assign({}, this.state, {
				[key]: value
			});

			let isValid =  this.ValidateForm(currentState);
			this.setState({
				[key]: value,
				disableSubmit: !isValid
			});
			// this.setState({disableSubmit: !this.ValidateForm()});	
		}
		
	}

	_validateOnFocusOut(value, input_type) {
		if(input_type && value !== "") {
			const errorMessage = validate(value, input_type);
			return  errorMessage; 
		}
	}

	_onSubmit(e) {
		console.log("Event submitted");
		console.log(this.state);
		let data = Object.assign({}, this.state);

		// delete button state since it's not part of the data
		delete data.disableSubmit
		
		alert("Ready to submit following data:\n" + JSON.stringify(data));
		e.preventDefault();
	}

	render() {
		return (
			<form>

				<Stack vertical tokens={VERTICAL_STACK_TOKEN}>
					<Stack horizontal tokens={HORIZONTAL_STACK_TOKEN}>
						<Title text="CLIENT DATA"/>
					</Stack>
					<Stack horizontal tokens={HORIZONTAL_STACK_TOKEN}>
						{this.GetUIComponentsByCategory(SITE_INFO)}
				    </Stack>
			        <Stack horizontal tokens={HORIZONTAL_STACK_TOKEN}>
				        {this.GetUIComponentsByCategory(CLIENT_INFO)}
				    </Stack>
				    <Stack horizontal tokens={HORIZONTAL_STACK_TOKEN}>
				        {this.GetUIComponentsByCategory(EXTRA_CLIENT_INFO)}
				    </Stack>
				    <Stack horizontal tokens={HORIZONTAL_STACK_TOKEN}>
					    <PrimaryButton
				            data-automation-id="submit"
				            text="Submit"
				            disabled={this.state.disableSubmit}
				            onClick={this._onSubmit}
				          />
				    </Stack>
				</Stack>

	        </form>
	       
        )
	}

}

export default Client