import React from 'react';
import { Separator } from 'office-ui-fabric-react/lib/Separator';
import { Stack } from 'office-ui-fabric-react';
import { Pivot, PivotItem, PivotLinkSize } from 'office-ui-fabric-react/lib/Pivot';

import Client from './honos_client_form.js';
import HonosForm from './honos_form.js';

import client_data from './data/client_data.json'
import honos_form_one from './data/honos_working.json'
import honos_form_two from './data/honos_learning.json'
import honos_form_three from './data/honos_child.json'
import './App.css';



function App() {
    return (
        <Stack vertical>
            <Client data={client_data}/>
            <Separator></Separator>
            <Pivot linkSize={PivotLinkSize.small}
                style={{padding: "15px"}}>
                <PivotItem headerText="HoNOS Working Age and 65">
                    <HonosForm data={honos_form_one}/>
                </PivotItem>
                <PivotItem headerText="HoNOS Learning Disabilities">
                    <HonosForm data={honos_form_two}/>
                </PivotItem>
                <PivotItem headerText="HoNOS Child and Adolescent">
                    <HonosForm data={honos_form_three}/>
                </PivotItem>
            </Pivot>
           
        </Stack>
    );
}

export default App;
